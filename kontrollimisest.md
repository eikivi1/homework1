# Väike ülevaade sellest, kuidas ma koduülesannet nr 1 läbi vaatan:

* Kõigepealt kloonin repo endale kohalikku arvutisse.
* Seejärel avan projekti PyCharmis.
* Vaatan PyCharmis, milline Teie koodi sisu on.
* Loon PyCharmis uue puhta virtuaalkeskkonna (et ei oleks teiste kodutööde pakette segamas).
* Aktiveerin loodud virtuaalkeskkonna PyCharmi terminalis ja Windowsi CommandPromptis.
* Paigaldan TestPyPi keskkonnast Teie loodud paketi virtuaalkeskkonda.
* Paigaldan PIPiga virtuaalkeskkonda 'pyproj' paketi, sest unustasin koduülesandesse kirjutada, et selle paigaldamine peaks toimuma automaatselt koos teie paketiga.
* Vaatan kus paiknevad Teie testid ja püüan neid windowsi konsoolis käivitada erinevate käskudega:
  * _nose2_
  * _pytest_
  * _python -m unittest discover_

* Väga hea oleks, kui Teil paikneb igas kaustas --init--.py' fail (alakriipsudega, mitte sidekriipudega), aga seda pahatihti ei ole. See võimaldab testimootoritel kaustadest ja alamkaustadest 'automaagiliselt' teste otsida.
Sageli ei taha kõik testimootorid teste ilusasti läbi jooksutada - probleemiks enamasti mingi import (ei leita importimiseks vajalikku faili või funktsiooni).
* Vaatan, kas ja milline on olemas kasutajaliides ning püüan seda konsoolis käivitada.
* Vaatan, kas tulemus on enam-vähem õige...
