# Homework 1 #
* Locations on earth are often expressed in geographic degrees (latitude and longitude). But when you are surveying you need to talk in meters (or feet). This is because - depending on the application - you use a geographic or projected coordinate system.

* A geographic coordinate system (GCS) is a coordinate system which uses a three-dimensional spherical surface (ellipsoid) to define locations on the earth. A common choice of coordinates is latitude and longitude. For example, main entrance of Taltech IT College is located on 59.395312° North and 24.664182° East in the WGS84 coordinate system.

* In a projected coordinate system (PCS) you project the geographic coordinate that you have measured, to, for example, a cylinder which you roll out easily on two-dimensional surface (the map). There exist many different projections. For example, main entrance of Taltech IT College is located on X=6584338.65 and Y=537735.47 in the Estonian L-Est97 coordinate system. 
NB X-axe direction is to North and Y-axe direction is to East.

* Rounding precision example: 59.395312°, 24.664182°; X=6584338.65 and Y=537735.47

* If there are differences of a few centimeters in the calculations, it does not matter.


## Task ##

* __Task1:__ You must write a module that converts coordinates from WGS84 to L-Est97 and vice versa, create install package and upload to test.pypi.org.

__Please don't use real PyPi repository!__ 

* __Task2:__ You must write a program (user interface) which uses created and installed(!) module for converting coordinates. The pip install command is something like this: pip install https://test.pypi.org/simple/transformations-username/
* Use bitbucket repository to share code with the lecturer (eikivi1 , eikivi@ttu.ee).
* Documentation and tests are mandatory.
*  When creating a module, be sure to pay attention to the contents of the setup.py, README.md and __init__py files.

### Grading ###
* 15 points + 5 extra points for using graphic user interface (GUI).
* Homework deadline: 17.02.2022

### Needed minimal documentation ###
* Readme Link to the module in the test environment as a single line text file: e.g.: https://test.pypi.org/project/transformations-username/

### Hints ###
* use pyproj library (https://pypi.org/project/pyproj/)
* Estonian coordinate system: http://spatialreference.org/ref/epsg/3301/
* WGS84 coordinate system: http://spatialreference.org/ref/epsg/4326/
* To prevent problems reinstalling the module, use underscores, not hyphens, in folder names in the tree structure.
Pay attention to tests naming convention 'test_***.py'
* Questions are welcome! Use Moodle forum or einar.kivisalu@taltech.ee


